<!DOCTYPE html>

<html lang="en">



<!-- Mirrored from themes.potenzaglobalsolutions.com/html/webster-responsive-multi-purpose-html5-template/index-01.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 May 2019 18:53:01 GMT -->

<head>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="keywords" content="HTML5 Template" />

<meta name="description" content="Webster - Responsive Multi-purpose HTML5 Template" />

<meta name="author" content="potenzaglobalsolutions.com" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<title>ASTAR Testing & Inspection</title>



<!-- Favicon -->

<link rel="shortcut icon" href="<?=base_url('resources/')?>images/favicon.ico" />



<!-- font -->

<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,500,500i,600,700,800,900|Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800">



<!-- Plugins -->

<link rel="stylesheet" type="text/css" href="<?=base_url('resources/')?>css/plugins-css.css" />



<!-- revoluation -->

<link rel="stylesheet" type="text/css" href="<?=base_url('resources/')?>revolution/css/settings.css" media="screen" />



<!-- Typography -->

<link rel="stylesheet" type="text/css" href="<?=base_url('resources/')?>css/typography.css" />



<!-- Shortcodes -->

<link rel="stylesheet" type="text/css" href="<?=base_url('resources/')?>css/shortcodes/shortcodes.css" />



<!-- Style -->

<link rel="stylesheet" type="text/css" href="<?=base_url('resources/')?>css/style.css" />

<link rel="stylesheet" type="text/css" href="<?=base_url('resources/')?>css/custom.css" />



<!-- Responsive -->

<link rel="stylesheet" type="text/css" href="<?=base_url('resources/')?>css/responsive.css" /> 



<!-- Style customizer -->

<link rel="stylesheet" type="text/css" href="#" data-style="styles"/>

<link rel="stylesheet" type="text/css" href="<?=base_url('resources/')?>css/style-customizer.css" />



<!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'../../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-557RCPW');</script>

<!-- End Google Tag Manager -->



</head>



<body>



<!-- Google Tag Manager (noscript) -->

<!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-557RCPW"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->

<!-- End Google Tag Manager (noscript) -->



<div class="wrapper"><!-- wrapper start -->



<!--=================================

 preloader -->

 

<div id="pre-loader">

    <img src="<?=base_url('resources/')?>images/pre-loader/loader-01.svg" alt="">

</div>



<!--=================================

 preloader -->



 

<!--=================================

 header -->



<header id="header" class="header default">

  <div class="topbar">

  <div class="container">

    <div class="row">

      <div class="col-lg-6 col-md-6 xs-mb-10">

        <div class="topbar-call text-center text-md-left">

          <ul>

            <li><i class="fa fa-envelope-o theme-color"></i> admin@astartesting.com.sg</li>

             <li><i class="fa fa-phone"></i> <a href="tel:+7042791249"> <span>(+65) 6261 6162 </span> </a> </li>

          </ul>

        </div>

      </div>

      <div class="col-lg-6 col-md-6">

        <div class="topbar-social text-center text-md-right">

          <ul>

            <li><a href="#"><span class="ti-facebook"></span></a></li>

            <!-- <li><a href="#"><span class="ti-instagram"></span></a></li>

            <li><a href="#"><span class="ti-google"></span></a></li>

            <li><a href="#"><span class="ti-twitter"></span></a></li> -->

            <li><a href="#"><span class="ti-linkedin"></span></a></li>

            <!-- <li><a href="#"><span class="ti-dribbble"></span></a></li> -->

          </ul>

        </div>

      </div>

     </div>

  </div>

</div>



<!--=================================

 mega menu -->



<div class="menu">  

  <!-- menu start -->

   <nav id="menu" class="mega-menu">

    <!-- menu list items container -->

    <section class="menu-list-items">

     <div class="container"> 

      <div class="row"> 

       <div class="col-lg-12 col-md-12"> 

        <!-- menu logo -->

        <ul class="menu-logo">

            <li>

                <a href="index-01.html"><img id="logo_imgs" src="<?=base_url('resources/')?>assets/images/logo.png" alt="logo"> </a>

            </li>

        </ul>

        <!-- menu links -->

        <div class="menu-bar">

         <ul class="menu-links">

           <li><a href="javascript:void(0)">Home </a></li>

           <li><a href="javascript:void(0)">Company </a></li>

           <li><a href="javascript:void(0)">Services </a></li>

           <li><a href="javascript:void(0)">Credentials </a></li>

           <li><a href="javascript:void(0)">Qualifications </a></li>

           <li><a href="javascript:void(0)">News & Events </a></li>

           <!-- <li><a href="javascript:void(0)">Enquiry </a></li>

           <li><a href="javascript:void(0)">Careers </a></li> -->

           <li><a href="javascript:void(0)">Contact us </a></li>

        </ul>

        </div>

       </div>

      </div>

     </div>

    </section>

   </nav>

  <!-- menu end -->

 </div>

</header>