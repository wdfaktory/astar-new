<?php $this->load->view('header'); ?>
 <section class="rev-slider">

  <div id="rev_slider_267_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webster-slider-1" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">

<!-- START REVOLUTION SLIDER 5.4.6.3 fullwidth mode -->

  <div id="rev_slider_267_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.6.3">

<ul>  <!-- SLIDE  -->

    <li data-index="rs-755" data-transition="random-static,random-premium,random" data-slotamount="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off"  data-randomtransition="on" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-masterspeed="default,default,default,default"  data-thumb="<?=base_url('resources/')?>revolution/assets/slider-01/1.jpg"  data-rotate="0,0,0,0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

    <!-- MAIN IMAGE -->

        <img src="<?=base_url('resources/')?>revolution/assets/slider-01/1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>

    <!-- LAYERS -->



    <!-- LAYER NR. 1 -->

    <div class="tp-caption   tp-resizeme" 

       id="slide-755-layer-1" 

       data-x="68" 

       data-y="center" data-voffset="-30" 

            data-width="['auto']"

      data-height="['auto']"

 

            data-type="text" 

      data-responsive_offset="on" 



            data-frames='[{"delay":1000,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'

            data-textAlign="['left','left','left','left']"

            data-paddingtop="[0,0,0,0]"

            data-paddingright="[0,0,0,0]"

            data-paddingbottom="[0,0,0,0]"

            data-paddingleft="[0,0,0,0]"



            style="z-index: 5; white-space: nowrap; font-size: 40px; line-height: 40px; font-weight: 200; color: rgba(255,255,255,1); font-family:Montserrat ;">Welcome to the world  of <br/> NDT </div>



    <!-- LAYER NR. 2 -->

    <div class="tp-caption   tp-resizeme  rev-color" 

       id="slide-755-layer-2" 

       data-x="right" data-hoffset="60" 

       data-y="center" data-voffset="-57" 

            data-width="['auto']"

      data-height="['auto']"

 

            data-type="text" 

      data-responsive_offset="on" 



            data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'

            data-textAlign="['left','left','left','left']"

            data-paddingtop="[0,0,0,0]"

            data-paddingright="[0,0,0,0]"

            data-paddingbottom="[0,0,0,0]"

            data-paddingleft="[0,0,0,0]"



            style="z-index: 6; white-space: nowrap; font-size: 150px; line-height: 150px; font-weight: 600; color: #ed1c24; font-family:Dosis;">ASTAR </div>



    <!-- LAYER NR. 3 -->

  <!--   <div class="tp-caption   tp-resizeme" 

       id="slide-755-layer-5" 

       data-x="center" data-hoffset="-353" 

       data-y="center" data-voffset="80" 

            data-width="['auto']"

      data-height="['auto']"

 

            data-type="text" 

      data-responsive_offset="on" 



            data-frames='[{"delay":3500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'

            data-textAlign="['left','left','left','left']"

            data-paddingtop="[0,0,0,0]"

            data-paddingright="[0,0,0,0]"

            data-paddingbottom="[0,0,0,0]"

            data-paddingleft="[0,0,0,0]"



            style="z-index: 8; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;text-transform:uppercase;"> Versatile  &nbsp; &nbsp;| </div> -->



    <!-- LAYER NR. 4 -->

   <!--  <div class="tp-caption   tp-resizeme" 

       id="slide-755-layer-7" 

       data-x="center" data-hoffset="-173" 

       data-y="center" data-voffset="80" 

            data-width="['auto']"

      data-height="['auto']"

 

            data-type="text" 

      data-responsive_offset="on" 



            data-frames='[{"delay":4000,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'

            data-textAlign="['left','left','left','left']"

            data-paddingtop="[0,0,0,0]"

            data-paddingright="[0,0,0,0]"

            data-paddingbottom="[0,0,0,0]"

            data-paddingleft="[0,0,0,0]"



            style="z-index: 10; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;text-transform:uppercase;"> Super Fast  &nbsp; &nbsp;| </div> -->



    <!-- LAYER NR. 5 -->

  <!--   <div class="tp-caption   tp-resizeme" 

       id="slide-755-layer-8" 

       data-x="center" data-hoffset="" 

       data-y="center" data-voffset="80" 

            data-width="['auto']"

      data-height="['auto']"

 

            data-type="text" 

      data-responsive_offset="on" 



            data-frames='[{"delay":4500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'

            data-textAlign="['left','left','left','left']"

            data-paddingtop="[0,0,0,0]"

            data-paddingright="[0,0,0,0]"

            data-paddingbottom="[0,0,0,0]"

            data-paddingleft="[0,0,0,0]"



            style="z-index: 12; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;text-transform:uppercase;"> Creative  &nbsp; &nbsp;| </div> -->



    <!-- LAYER NR. 6 -->

    <!-- <div class="tp-caption   tp-resizeme" 

       id="slide-755-layer-9" 

       data-x="center" data-hoffset="159" 

       data-y="center" data-voffset="80" 

            data-width="['auto']"

      data-height="['auto']"

 

            data-type="text" 

      data-responsive_offset="on" 



            data-frames='[{"delay":5000,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'

            data-textAlign="['left','left','left','left']"

            data-paddingtop="[0,0,0,0]"

            data-paddingright="[0,0,0,0]"

            data-paddingbottom="[0,0,0,0]"

            data-paddingleft="[0,0,0,0]"



            style="z-index: 14; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;text-transform:uppercase;">Modern  &nbsp; &nbsp; | </div> -->



    <!-- LAYER NR. 7 -->

   <!--  <div class="tp-caption   tp-resizeme" 

       id="slide-755-layer-10" 

       data-x="center" data-hoffset="302" 

       data-y="center" data-voffset="80" 

            data-width="['auto']"

      data-height="['auto']"

 

            data-type="text" 

      data-responsive_offset="on" 



            data-frames='[{"delay":5500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'

            data-textAlign="['left','left','left','left']"

            data-paddingtop="[0,0,0,0]"

            data-paddingright="[0,0,0,0]"

            data-paddingbottom="[0,0,0,0]"

            data-paddingleft="[0,0,0,0]"



            style="z-index: 16; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;text-transform:uppercase;">Elegant </div> -->



    <!-- LAYER NR. 8 -->

    <a class="tp-caption rev-btn  tp-resizeme  rev-button" 

 href="#" target="_self"       id="slide-755-layer-15" 

       data-x="center" data-hoffset="" 

       data-y="center" data-voffset="160" 

            data-width="['auto']"

      data-height="['auto']"

 

            data-type="button" 

      data-actions=''

      data-responsive_offset="on" 



            data-frames='[{"delay":2000,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power0.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(0,0,0);bs:solid;bw:0 0 0 0;"}]'

            data-textAlign="['inherit','inherit','inherit','inherit']"

            data-paddingtop="[12,12,12,12]"

            data-paddingright="[35,35,35,35]"

            data-paddingbottom="[12,12,12,12]"

            data-paddingleft="[35,35,35,35]"



            style="z-index: 17; white-space: nowrap; font-size: 12px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;text-transform:uppercase;background-color:#ed1c24;border-color:rgba(0,0,0,1);border-radius:3px 3px 3px 3px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Explore Our Services </a>

  </li>

  <!-- SLIDE  -->

    <li data-index="rs-756" data-transition="random-static,random-premium,random" data-slotamount="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off"  data-randomtransition="on" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-masterspeed="default,default,default,default"  data-thumb="<?=base_url('resources/')?>revolution/assets/slider-01/2.jpg"  data-rotate="0,0,0,0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

    <!-- MAIN IMAGE -->

        <img src="<?=base_url('resources/')?>revolution/assets/slider-01/2.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>

    <!-- LAYERS -->

    <!-- LAYER NR. 9 -->

    <div class="tp-caption   tp-resizeme"id="slide-756-layer-2"data-x="60"data-y="340"data-width="['auto']"data-height="['auto']"data-type="text"data-responsive_offset="on"data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'data-textAlign="['left','left','left','left']"data-paddingtop="[0,0,0,0]"data-paddingright="[0,0,0,0]"data-paddingbottom="[0,0,0,0]"data-paddingleft="[0,0,0,0]"style="z-index: 5; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 300; color: rgba(255,255,255,1); font-family:Montserrat ;">Are You </div>


    <!-- LAYER NR. 10 -->

    <div class="tp-caption   tp-resizeme"id="slide-756-layer-19"data-x="298"data-y="340"data-width="['auto']"data-height="['auto']"data-type="text"data-responsive_offset="on"data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'data-textAlign="['left','left','left','left']"data-paddingtop="[0,0,0,0]"data-paddingright="[0,0,0,0]"data-paddingbottom="[0,0,0,0]"data-paddingleft="[0,0,0,0]"style="z-index: 6; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;"> Ready </div>


    <!-- LAYER NR. 11 -->

    <div class="tp-caption   tp-resizeme"id="slide-756-layer-20"data-x="503"data-y="340"data-width="['auto']"data-height="['auto']"data-type="text"data-responsive_offset="on"data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'data-textAlign="['left','left','left','left']"data-paddingtop="[0,0,0,0]"data-paddingright="[0,0,0,0]"data-paddingbottom="[0,0,0,0]"data-paddingleft="[0,0,0,0]"style="z-index: 7; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 300; color: rgba(255,255,255,1); font-family:Montserrat ;">to </div>


    <!-- LAYER NR. 12 -->

    <div class="tp-caption   tp-resizeme" 

       id="slide-756-layer-3" 

       data-x="60" 

       data-y="center" data-voffset="85" 

            data-width="['auto']"

      data-height="['auto']"

 

            data-type="text" 

      data-responsive_offset="on" 



            data-frames='[{"delay":2090,"speed":1500,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'

            data-textAlign="['left','left','left','left']"

            data-paddingtop="[0,0,0,0]"

            data-paddingright="[0,0,0,0]"

            data-paddingbottom="[0,0,0,0]"

            data-paddingleft="[0,0,0,0]"



            style="z-index: 8; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 200; color: rgba(255,255,255,1); font-family: Montserrat ;">Lorem ipsum dolor sit amet, consectetur adipiscing elit.  <br/> Nunc non nisl eget felis finibus cursus non quis leo. </div>



    <!-- LAYER NR. 13 -->

    <a class="tp-caption rev-btn  tp-resizeme  rev-button" 

 href="https://themeforest.net/item/webster-responsive-multipurpose-html5-template/20904293?ref=Potenzaglobalsolutions" target="_self"       id="slide-756-layer-12" 

       data-x="60" 

       data-y="center" data-voffset="180" 

            data-width="['auto']"

      data-height="['auto']"

 

            data-type="button" 

      data-actions=''

      data-responsive_offset="on" 



            data-frames='[{"delay":3080,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power0.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(0,0,0);bc:rgb(0,0,0);bs:solid;bw:0 0 0 0;"}]'

            data-textAlign="['inherit','inherit','inherit','inherit']"

            data-paddingtop="[12,12,12,12]"

            data-paddingright="[30,30,30,30]"

            data-paddingbottom="[12,12,12,12]"

            data-paddingleft="[30,30,30,30]"



            style="z-index: 9; white-space: nowrap; font-size: 12px; line-height: 22px; font-weight: 700; color: #ffffff; font-family:Montserrat ;text-transform:uppercase;background-color:#ed1c24;border-color:rgba(0,0,0,1);border-radius:3px 3px 3px 3px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Discover More </a>



    <!-- LAYER NR. 14 -->

    <!-- <a class="tp-caption tp-resizeme" 

 href="https://themeforest.net/item/webster-responsive-multipurpose-html5-template/20904293?ref=Potenzaglobalsolutions" target="_self"       id="slide-756-layer-13" 

       data-x="235" 

       data-y="center" data-voffset="180" 

            data-width="['auto']"

      data-height="['auto']"

 

            data-type="button" 

      data-actions=''

      data-responsive_offset="on" 



            data-frames='[{"delay":3560,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power0.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,1);"}]'

            data-textAlign="['inherit','inherit','inherit','inherit']"

            data-paddingtop="[10,10,10,10]"

            data-paddingright="[30,30,30,30]"

            data-paddingbottom="[10,10,10,10]"

            data-paddingleft="[30,30,30,30]"



            style="z-index: 10; white-space: nowrap; font-size: 12px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;text-transform:uppercase;background-color:rgba(0,0,0,0);border-color:rgb(255,255,255);border-style:solid;border-width:2px 2px 2px 2px;border-radius:3px 3px 3px 3px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">purchase Now </a> -->



    <!-- LAYER NR. 15 -->

    <div class="tp-caption   tp-resizeme"id="slide-756-layer-15"data-x="60"data-y="center" data-voffset="6"data-width="['auto']"data-height="['auto']"data-type="text"data-responsive_offset="on"data-frames='[{"delay":1080,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'data-textAlign="['left','left','left','left']"data-paddingtop="[0,0,0,0]"data-paddingright="[0,0,0,0]"data-paddingbottom="[0,0,0,0]"data-paddingleft="[0,0,0,0]"style="z-index: 11; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 400; color: rgba(255,255,255,1); font-family:Montserrat ;">Just Change the </div>


    <!-- LAYER NR. 16 -->

    <div class="tp-caption   tp-resizeme" 

       id="slide-756-layer-17" 

       data-x="565" 

       data-y="419" 

            data-width="['auto']"

      data-height="['auto']"

 

            data-type="text" 

      data-responsive_offset="on" 



            data-frames='[{"delay":1080,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'

            data-textAlign="['inherit','inherit','inherit','inherit']"

            data-paddingtop="[0,0,0,0]"

            data-paddingright="[0,0,0,0]"

            data-paddingbottom="[0,0,0,0]"

            data-paddingleft="[0,0,0,0]"



            style="z-index: 13; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 600; color: #ffffff; letter-spacing: 0px;font-family:Montserrat;">Future </div>



    <!-- LAYER NR. 17 -->

    <div class="tp-caption   tp-resizeme" 

       id="slide-756-layer-18" 

       data-x="781" 

       data-y="420" 

            data-width="['auto']"

      data-height="['auto']"

 

            data-type="text" 

      data-responsive_offset="on" 



            data-frames='[{"delay":1080,"speed":1500,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'

            data-textAlign="['inherit','inherit','inherit','inherit']"

            data-paddingtop="[0,0,0,0]"

            data-paddingright="[0,0,0,0]"

            data-paddingbottom="[0,0,0,0]"

            data-paddingleft="[0,0,0,0]"



            style="z-index: 14; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Montserrat;">? </div>

  </li>

</ul>

<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>

</div>   

</section>


<section class="page-section-ptb">

  <div class="container">

    <div class="row"> 

      <div class="col-lg-4 align-self-center">
         <div class="section-title">
            <h6 class="subtitle">Why you'll love us? </h6>
            <h2 class="title-effect">Our <span class="theme-color"> Industries </span> </h2>
          </div>

          <p>We solve your most critical risks with standards.</p>

          <a class="button button-purple" href="#">Explore All Industries</a>

       </div>

       <div class="col-lg-8 sm-mt-40">

          <div class="row"> 

           <div class="col-md-6 col-sm-6">

             <div class="business-feature feature-text mt-30 ml-60 bg-overlay-black-50" style="background: url(images/about/1.jpg);">

             <div class="feature-icon position-relative">

              <span class="ti-panel text-white"></span>

              </div>

              <div class="feature-info mt-20 position-relative">

              <h5 class="text-white">Shipyard</h5>

               <p class="text-white">Cras volutpat dapibus elit congue tincidunt</p>

             </div>

           </div>

           <div class="business-feature feature-text mt-30 bg-overlay-black-50" style="background: url(<?=base_url('resources/')?>images/about/3.jpg);">

             <div class="feature-icon position-relative">

              <span class="ti-panel text-white"></span>

              </div>

              <div class="feature-info mt-20 position-relative">

              <h5 class="text-white">Oil, Gas & Oil Refinery</h5>

               <p class="text-white">Cras volutpat dapibus elit congue tincidunt</p>

             </div>

           </div>

          </div>

          <div class="col-md-6 col-sm-6">

             <div class="business-feature feature-text mt-30 bg-overlay-black-50" style="background: url(<?=base_url('resources/')?>images/about/2.jpg);">

             <div class="feature-icon position-relative">

              <span class="ti-shield text-white"></span>

              </div>

              <div class="feature-info mt-20 position-relative">

                  <h5 class="text-white">Petrochemical</h5>


              </div>

           </div>

           <div class="business-feature feature-text mt-30 n mr-30 bg-overlay-black-50" style="background: url(<?=base_url('resources/')?>images/about/13.jpg);">

             <div class="feature-icon position-relative">

              <span class="ti-shield text-white"></span>

              </div>

              <div class="feature-info mt-20 position-relative">

                  <h5 class="text-white">Power Plant</h5>

                   <p class="text-white">Cras volutpat dapibus elit congue tincidunt </p>

              </div>

           </div>

          </div>

         </div>

       </div>

      </div>

     </div>

  </section>


<section class="page-section-pb white-bg">
   <div class="container">
   <!--  <div class="row justify-content-center">
      <div class="col-lg-8">
       <div class="section-title text-center">
        <h2>Our<span class="theme-color"> Services </span> </h2>
        <p>Browse the most desirable categories and Explore some of the best tips from around the city from our partners and friends.</p>
       </div>
      </div>
    </div> -->
    <div class="row">

      <div class="col-md-8">

        <div class="section-title">

          <h6>We're Good At</h6>

          <h2 class="title-effect">Our <span class="theme-color"> Services </span> </h2>

          <p>Ut at tellus malesuada, volutpat est at, auctor ipsum. Quisque dolor arcu, pharetra tincidunt erat eu, sollicitudin faucibus neque.</p>

        </div>

      </div>

    </div>
   </div>
  <div class="container-fluid" id="services">
    <div class="row">
     <div class="col-12">
       <div class="owl-carousel" data-nav-dots="true" data-items="5" data-md-items="4" data-sm-items="3" data-xs-items="2" data-xx-items="1" data-space="20">
        <div class="item">
         <div class="blog-overlay">
         <div class="blog-image">
            <img class="img-fluid" src="<?=base_url('resources/')?>images/blog/small/01.jpg" alt="">
         </div>
         <div class="blog-name">
           <a class="tag" href="#"> 68 Listings </a>
            <h4 class="mt-15 text-white"><a href="#">Gym - Fitness</a></h4>
        </div>
      </div>        
     </div>
     <div class="item">
       <div class="blog-overlay">
         <div class="blog-image">
            <img class="img-fluid" src="<?=base_url('resources/')?>images/blog/small/02.jpg" alt="">
         </div>
         <div class="blog-name">
           <a class="tag" href="#"> 06 Listings </a>
            <h4 class="mt-15 text-white"><a href="#">Eat & drink</a></h4>
        </div>
      </div>        
     </div>        
     <div class="item">
       <div class="blog-overlay">
         <div class="blog-image">
            <img class="img-fluid" src="<?=base_url('resources/')?>images/blog/small/03.jpg" alt="">
         </div>
         <div class="blog-name">
           <a class="tag" href="#"> 58 Listings </a>
            <h4 class="mt-15 text-white"><a href="#">Art & History</a></h4>
        </div>
      </div>        
     </div>
     <div class="item">
       <div class="blog-overlay">
         <div class="blog-image">
            <img class="img-fluid" src="<?=base_url('resources/')?>images/blog/small/04.jpg" alt="">
         </div>
         <div class="blog-name">
           <a class="tag" href="#"> 150 Listings </a>
            <h4 class="mt-15 text-white"><a href="#">Shop - Store</a></h4>
        </div>
      </div>        
     </div>
     <div class="item">
       <div class="blog-overlay">
         <div class="blog-image">
            <img class="img-fluid" src="<?=base_url('resources/')?>images/blog/small/05.jpg" alt="">
         </div>
         <div class="blog-name">
           <a class="tag" href="#"> 10 Listings </a>
            <h4 class="mt-15 text-white"><a href="#">Conference and Event</a></h4>
        </div>
      </div>        
     </div>
     <div class="item">
       <div class="blog-overlay">
         <div class="blog-image">
            <img class="img-fluid" src="<?=base_url('resources/')?>images/blog/small/01.jpg" alt="">
         </div>
         <div class="blog-name">
           <a class="tag" href="#"> 15 Listings </a>
            <h4 class="mt-15 text-white"><a href="#">Outdoor activities</a></h4>
        </div>
       </div>        
      </div> 
     </div>        
    </div>
   </div>
  </div>
 </section>



<!-- <section class="page-section-ptb position-relative">

   <div class="container">

   <div class="row">

      <div class="col-md-8">

        <div class="section-title">

          <h6>We're Good At</h6>

          <h2 class="title-effect">Our Services</h2>

          <p>Ut at tellus malesuada, volutpat est at, auctor ipsum. Quisque dolor arcu, pharetra tincidunt erat eu, sollicitudin faucibus neque.</p>

        </div>

      </div>

    </div>

     <div class="row">

        <div class="col-md-4 xs-mb-40">

          <div class="feature-box h-100">

            <div class="feature-box-content">

            <h4>Rope Access</h4>

            <p>Nunc ipsum orci, faucibus semper malesuada eu, ullamcorper id risus. Aliquam finibus arcu pulvinar, posuere tellus.</p>

            </div>

            <a href="#">Read more</a>

            <div class="feature-box-img" style="background-image: url('<?=base_url('resources/')?>images/about/1.jpg');"></div>

            <span class="feature-border"></span>

          </div>

        </div>

        <div class="col-md-4 xs-mb-40">

          <div class="feature-box h-100 active">

            <div class="feature-box-content">

            <h4>RIG Inspection</h4>

            <p>Phasellus rhoncus porttitor dui, eu convallis odio maximus a. Pellentesque non bibendum erat. Duis maximus scelerisque.</p>

            </div>

            <a href="#">Read more</a>

            <div class="feature-box-img" style="background-image: url('<?=base_url('resources/')?>images/about/2.jpg');"></div>

            <span class="feature-border"></span>

          </div>

        </div>

        <div class="col-md-4">

          <div class="feature-box h-100">

            <div class="feature-box-content">

            <h4>Heat Treatment</h4>

            <p>Sed sem est, scelerisque venenatis placerat vitae, finibus sed est. Integer a neque eget tellus aliquam. Venenatis placerat vitae.</p>

            </div>

            <a href="#">Read more</a>

            <div class="feature-box-img" style="background-image: url('<?=base_url('resources/')?>images/about/3.jpg');"></div>

            <span class="feature-border"></span>

          </div>

        </div>

       </div>

   </div> 

</section> -->



<section class="page-section-ptb" id="home_content">

  <div class="container">

     <div class="row">

      <div class="col-lg-12">

        <div class="section-title">

            <h6>Who we are</h6>

            <h2 class="title-effect"> Global Leader in NDT.</h2>

            <p><strong>A-Star Testing & Inspection (S) Pte Ltd </strong> is a Leading Global UTM, NDT, Commissioning & Third party inspection company, Corporate Head office in Singapore and branches in Malaysia, Indonesia, India, Bangladesh, Ghana, Brazil and beyond.</p>

          </div> 

          <p>We have qualified Professionals Man power about 300 Technical Staffs certified in Advanced NDT Methods, Conventional NDT Methods, also combinations of NDT with Rope Access Qualifications, NDT with Under water Divers.</p>

          <p>The company is professionally equipped to carry out mechanical completion, Pre Com and commissioning work for the <span class="theme-color">Marine</span> | <span class="theme-color">Offshore</span> | <span class="theme-color">Oil & Gas</span> and Petro-chemical industries in Singapore and other regions.</p>

          <p>These services also performed by Rope Access, Rafting, under water which are all in accordance to top quality safety standards and are endorsed by many classification societies, Certification Bodies & Accreditation Counsels worldwide.</p>
      </div>  

      <div class="col-lg-6 sm-mt-30">

        <img class="img-fluid full-width" src="<?=base_url('resources/')?>images/about/12.jpg" alt="">

        <!-- <div class="owl-carousel" data-nav-arrow="true" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1">

          <div class="item">

             <img class="img-fluid full-width" src="images/about/12.jpg" alt="">

           </div>

          <div class="item">

            <img class="img-fluid full-width" src="images/about/13.jpg" alt="">

          </div>

          <div class="item">

            <img class="img-fluid full-width" src="images/about/1.jpg" alt="">

          </div>

         </div> -->

      </div>

     </div>

  </div>

</section>

<section class="page-section-ptb">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="section-title  text-center">
        <h2 class="title"> Our <span class="theme-bg"> Credentials  </span> </h2>
      </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4 col-lg-2 mt-30">
          <div class="job-box text-center">
             <img class="img-fluid d-block mx-auto" src="<?=base_url('resources/')?>images/credentials/abs.jpg" alt="">
             <strong><a class="mt-20 text-gray" href="#">Quality Management system Certification</a> </strong>
          </div>      
        </div>
        <div class="col-sm-4 col-lg-2 mt-30">
          <div class="job-box text-center">
             <img class="img-fluid d-block mx-auto" src="<?=base_url('resources/')?>images/credentials/sac-singlas.jpg" alt="">
             <strong><a class="mt-20 text-gray" href="#"> ISO/IEC 17025
Laboratory Accredited Certification</a> </strong>
          </div>      
        </div>
        
        <div class="col-sm-4 col-lg-2 mt-30">
          <div class="job-box text-center">
             <img class="img-fluid d-block mx-auto" src="<?=base_url('resources/')?>images/credentials/biz-safe.jpg" alt="">
             <strong><a class="mt-20 text-gray" href="#">WSH Council
Work Place Safety Certification</a> </strong>
          </div>      
        </div>
        <div class="col-sm-4 col-lg-2 mt-30">
          <div class="job-box text-center">
             <img class="img-fluid d-block mx-auto" src="<?=base_url('resources/')?>images/credentials/jas-anz.jpg" alt="">
             <strong><a class="mt-20 text-gray" href="#"> Health & Safety Management
System Certification</a> </strong>
          </div>      
        </div>
         <div class="col-sm-4 col-lg-2 mt-30">
          <div class="job-box text-center">
             <img class="img-fluid d-block mx-auto" src="<?=base_url('resources/')?>images/credentials/abs2.jpg" alt="">
             <strong><a class="mt-20 text-gray" href="#">American Bureau of
Shipping Class Approval</a> </strong>
          </div>      
        </div>
        <div class="col-sm-4 col-lg-2 mt-30">
          <div class="job-box text-center">
             <img class="img-fluid d-block mx-auto" src="<?=base_url('resources/')?>images/credentials/bvqi.jpg" alt="">
             <strong><a class="mt-20 text-gray" href="#">A-Star Testing & Inspection Bureau Veritas
Class Approval</a> </strong>
          </div>      
        </div>
<!-- 
        
        <div class="col-sm-4 col-lg-2 mt-30">
          <div class="job-box text-center">
             <img class="img-fluid d-block mx-auto" src="images/clients/awards/07.png" alt="">
             <strong><a class="mt-20 text-gray" href="#">Web guru</a> </strong>
          </div>      
        </div>
        <div class="col-sm-4 col-lg-2 mt-30">
          <div class="job-box text-center">
             <img class="img-fluid d-block mx-auto" src="images/clients/awards/08.png" alt="">
             <strong><a class="mt-20 text-gray" href="#">Boostlizer</a> </strong>
          </div>      
        </div>
        <div class="col-sm-4 col-lg-2 mt-30">
          <div class="job-box text-center">
             <img class="img-fluid d-block mx-auto" src="images/clients/awards/09.png" alt="">
             <strong><a class="mt-20 text-gray" href="#">Colorlib</a> </strong>
          </div>      
        </div>
        <div class="col-sm-4 col-lg-2 mt-30">
          <div class="job-box text-center">
             <img class="img-fluid d-block mx-auto" src="images/clients/awards/10.png" alt="">
             <strong><a class="mt-20 text-gray" href="#">CSSlight</a> </strong>
          </div>      
        </div>
        <div class="col-sm-4 col-lg-2 mt-30">
          <div class="job-box text-center">
             <img class="img-fluid d-block mx-auto" src="images/clients/awards/11.png" alt="">
             <strong><a class="mt-20 text-gray" href="#">DM</a> </strong>
          </div>      
        </div>
        <div class="col-sm-4 col-lg-2 mt-30">
          <div class="job-box text-center">
             <img class="img-fluid d-block mx-auto" src="images/clients/awards/01.png" alt="">
             <strong><a class="mt-20 text-gray" href="#">Awwwards</a> </strong>
          </div>      
        </div> -->
    </div>
    <div class="row"> 
        <a class="button button-purple" href="#">View All Credentials</a>

    </div>
  </div>
</section>
<section id="raindrops" class="raindrops" style="height: 50px;"></section>

<!--=================================

 footer -->

 <?php $this->load->view('footer'); ?>