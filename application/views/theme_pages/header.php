<!doctype html>

<html>

<!-- Mirrored from theme.innovatory.in/restora/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Apr 2019 12:26:24 GMT -->

<head>

   <meta charset="utf-8">

   <meta name="viewport" content="width=device-width, initial-scale=1">

   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

   <title>Fantasy</title>

   <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet" type="text/css">

   <link rel="icon" href="assets/img/logo-fantasy.jpg" sizes="16x16">

   <link href="<?=base_url()?>assets/css/jquery.colorpanel.css" rel="stylesheet">

   <link href="<?=base_url()?>assets/css/skins-default.css" id="cpswitch" rel="stylesheet">

   <link href="<?=base_url()?>assets/css/bootstrap.css" rel="stylesheet" type="text/css">

   <link href="<?=base_url()?>assets/font/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900" rel="stylesheet">

   <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">

   <link href="<?=base_url()?>assets/font/font/flaticon.css" rel="stylesheet" type="text/css">

   <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/mega_menu.css"/>

   <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/settings.css">

   <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/navigation.css">

   <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/slider.css"/>

   <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/responsive.css">

   <link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.bxslider.css" type="text/css"/>

   <link href="<?=base_url()?>assets/css/animate.html" rel="stylesheet">

   <link href="<?=base_url()?>assets/css/tilt.css" rel="stylesheet" type="text/css">

   <link href="<?=base_url()?>assets/css/carousel.min.css" rel="stylesheet">

   <link href="<?=base_url()?>assets/css/theme.min.css" rel="stylesheet">

   <link href="<?=base_url()?>assets/css/transitions.css" rel="stylesheet">

   <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/image-grid.css"/>

   <script type="text/javascript" src="<?=base_url()?>assets/js/modernizr.custom.26633.js"></script>

   <noscript>

      <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/fallback.css"/>

   </noscript>

   <script type="text/javascript">

     document.addEventListener('DOMContentLoaded', function () {

     let dropdownList = document.getElementsByClassName('drop-down-multilevel'); 

     let dropdownListChildren = dropdownList[0].children;

      let z = document.getElementsByClassName('menu-li-a-drop');

      for(let i=0;i<z.length;i++){

         z[i].addEventListener('click', function(){

            let dropdownListChildTagEle = dropdownListChildren[i].children;

            dropdownListChildTagEle = dropdownListChildTagEle[0].href.split('#');

            let na = document.getElementsByClassName('sld-btn-bor');

            let loc = window.location.href.split('#');

            for(let i=0;i<na.length;i++){

               let curr = na[i].href.split('#');

               if (curr[1] == dropdownListChildTagEle[1]){

                  na[i].click();

               }

            }      

         })

      }

      let na = document.getElementsByClassName('sld-btn-bor');

      let loc = window.location.href.split('#');

      for(let i=0;i<na.length;i++){

         let curr = na[i].href.split('#');

         if (curr[1] == loc[1]){

            na[i].click();

         }

      }      

      window.addEventListener('scroll', function() {

         console.log(document.documentElement.scrollTop);

            let getClass = document.getElementsByClassName('topnav');

         if (document.documentElement.scrollTop > 280) {

            getClass[0].classList.add('fixedtopnav');
            getClass[0].style.opacity = '1';

         } else if (document.documentElement.scrollTop > 0 && document.documentElement.scrollTop < 280) {
            getClass[0].style.opacity = '0';            
         }  else if (document.documentElement.scrollTop == 0) {
            getClass[0].style.opacity = '1';
        } else {
            getClass[0].classList.remove('fixedtopnav')

         }

      });

   });

   </script>

   <style type="text/css">

      .topnav{

         /*border-bottom: 1px solid #ccc;*/

         padding-bottom: 5px;

         color: #333;

         font-size: 14px;
         position: fixed;
    top: 5px;
    left: 0;
    right: 0;
    z-index: 9991;

      }
      .topnav ul li span i{
         background-color: #333;
    padding: 5px 6px;
    border-radius: 50%;
    color: #ff5722;
      }

   </style>

</head>

<body>

<div id="back-to-top"> <a class="top arrow" href="#top"><i class="fa fa-long-arrow-up"></i></a></div>

<div id="loading">

   <div id="preloader"></div>

</div>

<!-- <div id="colorPanel" class="colorPanel">

   <a id="cpToggle" href="#"></a> 

   <ul>

      <li><a class="linka" href="<?=base_url()?>assets/css/skins-default.css" style="background-color: rgb(255, 87, 34);"></a></li>

   </ul>

</div> -->

<div id="wrapper">

<header id="header" class="header">

   <div class="menu">

      <nav class="topnav container topvav-1">

         <ul>

            <li><span><i class="fa fa-phone mw" aria-hidden="true"></i> +960 3224668</span></li>

            <li><span><i class="fa fa-envelope mw" aria-hidden="true"></i> admin@fantasy.com.mv</span></li>

         </ul>

      </nav>

      <nav id="menu-1" class="mega-menu ">

         <section class="menu-list-items">

            <div class="container">

               <div class="row">

                  <div class="col-lg-12 col-md-12">

                     <ul class="menu-logo">

                        <li class="head-info"> <a href="<?=base_url()?>"><img id="logo_img" src="<?=base_url()?>/assets/img/logo-fantasy.jpg" alt="logo"> </a> </li>

                     </ul>

                     <ul class="menu-links">

                        <!-- <li><a class="menu-style menu-li-a" href="#">HOME</a></li> -->

                        <?php if($this->uri->segment('1') == "about-us"){ 

                           $aboutactive = "active";

                        } else { 

                           $aboutactive = "";

                        } 

                        if($this->uri->segment('1') == "products"){

                           $productactive = "active";

                        } else { 

                           $productactive = "";

                        }

                        if($this->uri->segment('1') == "outlets"){

                           $outletsactive = "active";

                        } else { 

                           $outletsactive = "";

                        }

                        if($this->uri->segment('1') == "news-and-events"){

                           $newsactive = "active";

                        } else { 

                           $newsactive = "";

                        }

                        if($this->uri->segment('1') == "contact"){

                           $contactactive = "active";

                        } else { 

                           $contactactive = "";

                        }

                        ?>

                        <li><a class="menu-style menu-li-a <?=$aboutactive ?>" href="<?=base_url('about-us')?>">ABOUT US</a></li>

                        <li>

                           <a class="menu-style menu-li-a <?=$productactive ?>" href="<?=base_url('products')?>"> PRODUCTS <i class="fa fa-angle-down fa-indicator"></i></a> 

                           <ul class="drop-down-multilevel">

                              <li><a class="menu-li-a-drop" href="<?=base_url('products#fruits')?>">Fruits & Vegetables</a></li>

                              <li><a class="menu-li-a-drop" href="<?=base_url('products#pastries')?>">Pastries</a></li>

                              <li><a class="menu-li-a-drop" href="<?=base_url('products#beverages')?>">Beverages</a></li>

                              <li><a class="menu-li-a-drop" href="<?=base_url('products#meats')?>">Meats</a></li>

                              <li><a class="menu-li-a-drop" href="<?=base_url('products#seafood')?>">Seafoods</a></li>

                              <li><a class="menu-li-a-drop" href="<?=base_url('products#others')?>">Others</a></li>

                           </ul>

                        </li>

                        <li><a class="menu-style menu-li-a <?=$outletsactive ?>" href="<?=base_url('outlets')?>">OUTLETS </a></li>

                        <li><a class="menu-style menu-li-a <?=$newsactive ?>" href="<?=base_url('news-and-events')?>">NEWS & EVENTS </a></li>

                        <li><a class="menu-style menu-li-a <?=$contactactive ?>" href="<?=base_url('contact')?>">CONTACT US</a></li>

                        <!-- <li><a class="menu-style menu-li-a" href="#">CAREERS </a></li> -->

                        <li><a class="menu-li-a" href="#search"><i class="fa fa-search" aria-hidden="true"></i></a></li>

                     </ul>

                  </div>

               </div>

            </div>

         </section>

      </nav>

   </div>

</header>